import RPi.GPIO as GPIO
import time
LED=17
GPIO.setmode(GPIO.BCM)
GPIO.setup(LED, GPIO.OUT)
GPIO.output(LED, GPIO.LOW)

BUTTON = 23

GPIO.setup(BUTTON,GPIO.IN,pull_up_down=GPIO.PUD_UP)

try:
    while True:
        print('In while loop')
        button_state = GPIO.input(BUTTON)
        if button_state:
            time.sleep(0.05)
            GPIO.output(LED, GPIO.LOW)
        else:
            print('button Pressed' + str(LED) + ' button ' + str(BUTTON))
            GPIO.output(LED, GPIO.HIGH)
except KeyboardInterrupt:
    GPIO.cleanup()