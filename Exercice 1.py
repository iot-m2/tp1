import RPi.GPIO as GPIO
import time
LED=17
FRESH=0.5
GPIO.setmode(GPIO.BCM)
GPIO.setup(LED, GPIO.OUT)
GPIO.output(LED, GPIO.LOW)
try:
    while True:
        print('In while loop')
        GPIO.output(LED, GPIO.HIGH)
        time.sleep(FRESH)
        GPIO.output(LED, GPIO.LOW)
        time.sleep(FRESH)
except KeyboardInterrupt:
    GPIO.cleanup()