import RPi.GPIO as GPIO
import time
LED=17
GPIO.setmode(GPIO.BCM)
GPIO.setup(LED, GPIO.OUT)
GPIO.output(LED, GPIO.LOW)

BUTTON = 23

GPIO.setup(BUTTON,GPIO.IN,pull_up_down=GPIO.PUD_UP)

try:
    nextState = True
    while True:
        print('In while loop')
        if not GPIO.input(BUTTON):
            print('button Pressed' + str(LED) + ' button ' + str(BUTTON))
            GPIO.output(LED, nextState)
            nextState = not nextState
            while not GPIO.input(BUTTON):
                time.sleep(0.1) # sleep 1 second
        else:
            time.sleep(0.05)
except KeyboardInterrupt:
    GPIO.cleanup()