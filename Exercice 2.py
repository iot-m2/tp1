import RPi.GPIO as GPIO
import time
LED=17
x = input('Enter a value')
FRESH= int(x)
GPIO.setmode(GPIO.BCM)
GPIO.setup(LED, GPIO.OUT)
GPIO.output(LED, GPIO.LOW)
try:
    while True:
        GPIO.output(LED, GPIO.HIGH)
        time.sleep(FRESH)
        GPIO.output(LED, GPIO.LOW)
        time.sleep(FRESH)
except KeyboardInterrupt:
    GPIO.cleanup()