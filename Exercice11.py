import RPi.GPIO as GPIO
import time

FRESH=2 #Time sleep (s)
BUZZER=18
LDR=23 #Light sensors
LED=24

GPIO.setmode(GPIO.BCM)

GPIO.setup(BUZZER, GPIO.OUT)
GPIO.output(BUZZER, GPIO.HIGH)

GPIO.setup(LDR, GPIO.IN , pull_up_down = GPIO.PUD_DOWN)

GPIO.setup(LED, GPIO.OUT)
GPIO.output(LED, False)

try:
    while True:
        if GPIO.input(LDR) == GPIO.HIGH:
            print("light up")
            GPIO.output(LED, False)
        else:
            print("light down")
            GPIO.output(BUZZER,GPIO.HIGH)
            GPIO.output(LED, True)
            time.sleep(FRESH)
            GPIO.output(BUZZER,GPIO.LOW)
except KeyboardInterrupt:
    GPIO.cleanup        