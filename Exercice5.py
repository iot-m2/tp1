import RPi.GPIO as GPIO
import time
LEDS = [2, 3, 4]
BUTTONS = [17, 27, 22]

GPIO.setmode(GPIO.BCM)

def setup_component(pin, output_value):
    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(pin, output_value)

for led in LEDS:
    setup_component(led, GPIO.LOW)

for button in BUTTONS:
    GPIO.setup(button,GPIO.IN,pull_up_down=GPIO.PUD_UP)

def on_click(BUTTON, LED):
    if not GPIO.input(BUTTON):
        print('button Pressed' + str(LED) + ' button ' + str(BUTTON))
        GPIO.output(LED, GPIO.HIGH)
        while not GPIO.input(BUTTON):
            time.sleep(0.1) # sleep 1 second
    else:
        time.sleep(0.05)
        GPIO.output(LED, GPIO.LOW)

try:
    while True:
        print('In while loop')
        for i in [0, 1, 2]:
            on_click(BUTTONS[i], LEDS[i])
except KeyboardInterrupt:
    GPIO.cleanup()