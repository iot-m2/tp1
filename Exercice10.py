import RPi.GPIO as GPIO
import time
RED_LED = 2
ORANGE_LED = 3
GREEN_LED = 4
BUZZER=17

GPIO.setmode(GPIO.BCM)

def setup_component(pin, output_value):
    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(pin, output_value)

setup_component(RED_LED, GPIO.LOW)
setup_component(ORANGE_LED, GPIO.LOW)
setup_component(GREEN_LED, GPIO.LOW)
setup_component(BUZZER, GPIO.LOW)

def beepXnumber(x):
    i = 0
    FRESH = 0.15
    while x > i:
        GPIO.output(BUZZER, True)
        time.sleep(FRESH)
        GPIO.output(BUZZER, False)
        time.sleep(FRESH)
        i = i + 1
    return x * 2 * FRESH

try:
    while True:
        GPIO.output(ORANGE_LED, False)
        GPIO.output(RED_LED, True)
        print('Red led is on')
        sleeped_time = beepXnumber(3)
        time.sleep(1 - sleeped_time)
        GPIO.output(RED_LED, False)
        GPIO.output(GREEN_LED, True)
        print('Green led is on')
        sleeped_time = beepXnumber(1)
        time.sleep(1 - sleeped_time)
        GPIO.output(GREEN_LED, False)
        GPIO.output(ORANGE_LED, True)
        print('Orange led is on')
        sleeped_time = beepXnumber(2)
        time.sleep(1 - sleeped_time)
except KeyboardInterrupt:
    GPIO.cleanup()