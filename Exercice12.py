import RPi.GPIO as GPIO
import time
from random import randrange

FRESH=2 #Time sleep (s)

LED_RED = 16
LED_BLUE = 20
LED_GREEN = 21

GPIO.setmode(GPIO.BCM)

GPIO.setup(LED_RED, GPIO.OUT)
GPIO.output(LED_RED, False)

GPIO.setup(LED_BLUE, GPIO.OUT)
GPIO.output(LED_BLUE, False)

GPIO.setup(LED_GREEN, GPIO.OUT)
GPIO.output(LED_GREEN, False)

def change_state(STATE, LED):
    if STATE:
        print('LED turned ON')
        GPIO.output(LED, GPIO.HIGH)
    else:
        print('LED turned OFF')
        GPIO.output(LED, GPIO.LOW)

def random_color():
    random_int = randrange(3)
    if random_int == 0:
        change_state(True,LED_RED)
    if random_int == 1:
        change_state(True,LED_BLUE)
    if random_int == 2:
        change_state(True,LED_GREEN)

try:
    while True:
        print("MENU")
        print("1. Red ON")
        print("2. Red OFF")
        print("3. Blue ON")
        print("4. Blue OFF")
        print("5. Green ON")
        print("6. Green OFF")
        print("7. TURN ON ALL")
        print("8. Turn OFF ALL")
        print("9. Random color")
        print("0. Exit")

        val = input("Enter your choice: ") 
        if val == 1:
            change_state(True,LED_RED)
            continue
        elif val == 2:
            change_state(False,LED_RED)
            continue
        elif val == 3:
            change_state(True,LED_BLUE)
            continue
        elif val == 4:
            change_state(False,LED_BLUE)
            continue
        elif val == 5:
            change_state(True,LED_GREEN)
            continue
        elif val == 6:
            change_state(False,LED_GREEN)
            continue
        elif val == 7:
            change_state(True,LED_RED)
            change_state(True,LED_BLUE)
            change_state(True,LED_GREEN)
            continue
        elif val == 8:
            change_state(False,LED_RED)
            change_state(False,LED_BLUE)
            change_state(False,LED_GREEN)
            continue
        elif val == 9:
            random_color()
            continue
        elif val == 0:
            print("END")
            break
        else:
            print("Wrong choice ...")
            continue
except KeyboardInterrupt:
    GPIO.cleanup    