import RPi.GPIO as GPIO
import time
RED_LED = 2
ORANGE_LED = 3
GREEN_LED = 4

GPIO.setmode(GPIO.BCM)

def setup_component(pin, output_value):
    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(pin, output_value)

setup_component(RED_LED, GPIO.LOW)
setup_component(ORANGE_LED, GPIO.LOW)
setup_component(GREEN_LED, GPIO.LOW)

try:
    while True:
        GPIO.output(ORANGE_LED, False)
        GPIO.output(RED_LED, True)
        print('Red led is on')
        time.sleep(1)
        GPIO.output(RED_LED, False)
        GPIO.output(GREEN_LED, True)
        print('Green led is on')
        time.sleep(0.5)
        GPIO.output(GREEN_LED, False)
        GPIO.output(ORANGE_LED, True)
        print('Orange led is on')
        time.sleep(0.2)
except KeyboardInterrupt:
    GPIO.cleanup()