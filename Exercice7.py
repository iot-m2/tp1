import RPi.GPIO as GPIO
import time
RED_LED = 2
ORANGE_LED = 3
GREEN_LED = 4
BUTTON = 17

GPIO.setmode(GPIO.BCM)

def setup_component(pin, output_value):
    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(pin, output_value)

setup_component(RED_LED, GPIO.LOW)
setup_component(ORANGE_LED, GPIO.LOW)
setup_component(GREEN_LED, GPIO.LOW)
GPIO.setup(BUTTON,GPIO.IN,pull_up_down=GPIO.PUD_UP)

try:
    while True:
        GPIO.output(RED_LED, True)
        GPIO.output(ORANGE_LED, False)
        print('Red led is on')
        if not GPIO.input(BUTTON):
            print('button Pressed' + str(BUTTON) + ' button ' + str(BUTTON))
            GPIO.output(RED_LED, False)
            GPIO.output(GREEN_LED, True)
            print('Green led is on')
            time.sleep(1)
            GPIO.output(GREEN_LED, False)
            GPIO.output(ORANGE_LED, True)
            print('Orange led is on')
            time.sleep(0.2)
        else:
            time.sleep(0.05)
except KeyboardInterrupt:
    GPIO.cleanup()