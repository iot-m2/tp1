import RPi.GPIO as GPIO
import time
import random

RED_LED = 2
BLUE_LED = 3
GREEN_LED = 4

GPIO.setmode(GPIO.BCM)

def setup_component(pin, output_value):
    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(pin, output_value)

setup_component(RED_LED, GPIO.LOW)
setup_component(BLUE_LED, GPIO.LOW)
setup_component(GREEN_LED, GPIO.LOW)

try:
    random_number = int(random.random() * 20)
    attempts_number = 0
    while True:
        if attempts_number == 4:
            print("Sorry, you didn't find the secret number after 4 attempts!")
            break
        while True:
            user_number = input("Choose an int number between 1 and 20")
            if type(user_number) is int and user_number < 21 and user_number > 0:
                break
        
        if random_number == user_number:
            GPIO.output(GREEN_LED, True)
            GPIO.output(RED_LED, False)
            GPIO.output(BLUE_LED, False)
            print("Great, this is the right number")
            time.sleep(2)
            break
        elif random_number > user_number:
            GPIO.output(GREEN_LED, False)
            GPIO.output(RED_LED, False)
            GPIO.output(BLUE_LED, True)
            print("The secret number is greater than that..")
        else:
            GPIO.output(GREEN_LED, False)
            GPIO.output(RED_LED, True)
            GPIO.output(BLUE_LED, False)
            print("The secret number is lower than that..")
        attempts_number = attempts_number + 1
except KeyboardInterrupt:
    GPIO.cleanup()