import RPi.GPIO as GPIO
import time
BUZZER=17
GPIO.setmode(GPIO.BCM)

def setup_component(pin, output_value):
    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(pin, output_value)

setup_component(BUZZER, GPIO.LOW)

BUTTON = 27

GPIO.setup(BUTTON,GPIO.IN,pull_up_down=GPIO.PUD_UP)

try:
    while True:
        print('In while loop')
        if not GPIO.input(BUTTON):
            print('button Pressed' + str(BUZZER) + ' button ' + str(BUTTON))
            GPIO.output(BUZZER, GPIO.HIGH)
            time.sleep(0.5)
            GPIO.output(BUZZER, GPIO.LOW)
            while not GPIO.input(BUTTON):
                time.sleep(0.1) # sleep 1 second
except KeyboardInterrupt:
    GPIO.cleanup()