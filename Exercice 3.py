import RPi.GPIO as GPIO
import time
LEDS = [17, 27]
FRESH=0.5
index  = 0

GPIO.setmode(GPIO.BCM)
for LED in LEDS:
    GPIO.setup(LED, GPIO.OUT)
    GPIO.output(LED, GPIO.LOW)

try:
    while True:
        GPIO.output(LEDS[index], GPIO.LOW)
        GPIO.output(LEDS[(index + 1)%2], GPIO.HIGH)
        time.sleep(FRESH)
        index = (index + 1)%2
except KeyboardInterrupt:
    GPIO.cleanup()
